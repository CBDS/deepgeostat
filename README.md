# DeepGeoStat

This is the repository for the DeepGeoStat project. DeepGeoStat uses Docker containers to run its services.
The project consists of two main components for which separate Docker containers are built:

  * The web application;
  * The API.

# Setup

## Automated script
To set up all the required docker containers, you can run the automated shell script `setup.sh` in the `src/` folder.
Navigate to the `src` in the unix terminal and run the following command. **Make sure you run the command with sudo.**:

```shell
sudo sh setup.sh
```

In case you want to complete **nuke all** your existing docker containers, you can append the `--force` or shorthand `-f` flag to the command likek the following below.
**WARNING: THIS OPTION REMOVES ALL YOUR EXISTING DOCKER CONTAINERS SAVED BY DOCKER.**.

```shell
sudo sh setup.sh --force
```

The script should automate the complete process and setup up all the Docker containers.

In case any of the steps in the build script fails, you can try to run the build scripts separately for the web-app and API containers:

```shell
# Setup docker containers for the webapp
# In the src/web-app folder:
sudo sh build_web_app.sh
```

```shell
# Setup docker containers for the api
# In the src/experiments-api folder:
sudo sh build_api.sh
```
## Manual install
If the automated script happens to fail, you can follow the steps below to troubleshoot or to manually build the docker containers from scratch.

1. First of all, clone the directory onto your local machine:
```shell
git clone git@gitlab.com:CBDS/deepgeostat.git
```

### Adding grid image directory
1. Make sure to add the directory named `images` with grid image in the `src/experiments-api` folder. The images are therefore expected to be found in:
```shell
src/experiments-api/images/
```

### Setting up the Web App
1. Navigate to the `web-app` folder and copy `.env.default` to `.env`.
```shell
cd web-app \
&& cp .env.default .env
```

2. Build an installer Docker container.
```shell
docker compose -f docker-compose.dev.yml up installer -d
```

3. Run the following commands to install the PHP dependencies via `composer` and `npm`.
```shell
docker exec web-app-installer composer install
docker exec web-app-installer npm install
```

4. Stop `installer` container.
```shell
docker compose -f docker-compose.dev.yml --profile installer down
```

5. Launch the Docker container for the web-app.
```shell
docker compose -f docker-compose.dev.yml up -d
```

6. Generate the artisan key required for Laravel.
```shell
docker exec --user 0 web-app-serve-1 /bin/bash -c "chown -R www-data:www-data /var/www" \
&& docker exec web-app-serve-1 /bin/bash -c "php artisan key:generate"
```

7. Load the data onto your docker container.
```shell
docker exec web-app-serve-1 php artisan migrate --seed
```

The containers for the web-app should be now completely set up.

### Setting up the API
1. Navigate to the `experiments-api` and copy the `.env.default` file to `.env`.
```shell
cd experiments-api \
&& cp .env.default .env
```

2. Lastly, simply run the following command to launch the Docker container.
```shell
docker compose -f docker-compose.dev.yml up -d
```
