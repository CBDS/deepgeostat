#!/usr/bin/env sh
DOCKER_COMPOSE_FILENAME="docker-compose.dev.yml"
ENV_FILENAME=".env"

(test -f $ENV_FILENAME && echo ".env file found for the API" || (cp "$ENV_FILENAME.default" $ENV_FILENAME) && chmod 775 $ENV_FILENAME) \
&& docker compose -f $DOCKER_COMPOSE_FILENAME up -d;

test $? -eq 0 && echo ">> API build successful!" || (echo ">> API build failed!" && exit 1)

